package test_lib

import (
	"fmt"
)

func Divide(num1, num2 int) (int, error) {
	var res int
	if num2 == 0 {
		return res, fmt.Errorf("number can not zero %d", num2)
	}
	res = num1 / num2
	return res, nil
}

func Multiply(num1, num2 int) (int, error) {
	return num1 * num2, nil
}

func Add(num1, num2 int) (int, error) {
	return num1 + num2, nil
}

func Subtract(num1, num2 int) (int, error) {
	return num1 - num2, nil
}
